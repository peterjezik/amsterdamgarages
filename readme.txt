How to install and run the application:

1, prerequisites: Node.js, npm and git installed
2, install bower ("npm install -g bower")
3, clone the repository ("git clone https://peterjezik@bitbucket.org/peterjezik/amsterdamgarages.git")
4, cd into the directory ("cd amsterdamgarages")
5, install packages from package.json (right now only lite-server) ("npm install")
6, install packages from bower.json (right now only angular#1.5) ("bower install")
7, run the server ("npm start")
8, browser with the application should be opened; if not go to localhost:3000 in a browser window

Architectural decisions:

- bower used as a package manager for front-end application because it creates flat dependency tree (in contrast with npm)
- npm used just to install the lite-server for development/demo purposes
- whole application added into /public directory with index.html as an entry point
- two main components used: list of garages (garagesList) and map (map)
- both components added later into separate modules, because even though it is a small application it was becoming quite unmaintainable even in this stage while residing in one module
- html part was also moved from the template property of the component into separate html file (even though e.g. map template has only one line of code) for maintanance and possible future development purposes
- garagesList component is responsible for getting the data from the public source and displaying it on the page
- there are two types of parking places: long and short. I decided, for the sake of clarity and easy of use of the application, to show only the overal status in the garages list (i.e. the garage is free when there are either short or long parking places free, and the garage is full when there are both short and long parking places full) and the details about the types and availability of parking places is shown on the map after clicking the garage in question
- map component is responsible for displaying the initial map and displaying the position of the chosen garage together with the details about the parking places
- the map itself together with the markers and info windows is created in the mapService, which is also reachable from the garagesList component and serves as a communication channel between the garagesList component and map component
