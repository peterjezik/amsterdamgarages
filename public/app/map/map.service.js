angular
    .module("map")
    .factory("mapService", function(){

        var self = this;
        var map;
        var marker;
        var infoWindow;
        
        //find the garage on the map and display the details about it
        findGarage = function(garage){
            //get the data from the garage object
            var data = garage.properties.layers["parking.garage"].data;
            var freeSpaceShort = data.FreeSpaceShort;
            var freeSpaceLong = data.FreeSpaceLong;
            var shortCapacity = data.ShortCapacity;
            var longCapacity = data.LongCapacity;
            var title = garage.properties.title;
            var lng = garage.geometry.coordinates[0];
            var lat = garage.geometry.coordinates[1];
            var position = {lat: lat, lng: lng};
            //create the content of the info window displayed on the map
            var markerContent = "<h4>" + title + "</h4>" +
                                "<p>Free space long: " + freeSpaceLong + "/" + longCapacity + "</p>" + 
                                "<p>Free space short: " + freeSpaceShort + "/" + shortCapacity + "</p>";
            //get the references (if any) of previously created map, marker and info window
            var map = self.map;
            var marker = self.marker;
            var infoWindow = self.infoWindow;
            //remove previously existed info window (if any)
            if(infoWindow) infoWindow.close();
            //create new info window
            self.infoWindow = new google.maps.InfoWindow();
            //remove previously existed marker (if any)
            if(marker) marker.setMap(null);
            //create the new marker
            self.marker = new google.maps.Marker(
                {
                    position: position,
                    map: map,
                    title: title
                }
            )
            //create the new info window
            self.infoWindow.setContent(markerContent);
            self.infoWindow.open(map, self.marker);
        }

        //create the map on the given element with given coordinates and zoom
        createMap = function(elementId, lat, lng, zoom){
            //create the map
            var map = new google.maps.Map(document.getElementById(elementId), {
                zoom: zoom,
                center: {lat: lat, lng: lng}
            });
            //store the reference to the map
            self.map = map;
        }

        //expose the service methods
        return {
            findGarage: findGarage,
            createMap: createMap
        };
})