angular
    .module("map")
    .component("map", {
        templateUrl: "app/map/map.template.html",
        controller: function mapController(mapService){
            //create the initial view of the map, centered on Amsterdam        
            mapService.createMap("map", 52.3745913, 4.8285751, 11);
        }
    });