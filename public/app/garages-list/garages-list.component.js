angular
    .module("garagesList")
    .component("garagesList", {
        templateUrl: "app/garages-list/garages-list.template.html",
        controller: function garageListController($http, mapService){
            var self = this;

            //get the garages data from public API
            $http.get('http://api.citysdk.waag.org/layers/parking.garage/objects?per_page=25')
                .then(function(response){self.garages = response.data});
            
            //locate the garage on the map and display the details about it
            self.findGarage = function(garage){
                mapService.findGarage(garage);
            }
        }
    });